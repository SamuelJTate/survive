# README #

This README documents the steps that are necessary to get SURVIVE up and running.

### What is this repository for? ###

* This repo holds the latest version of my game SURVIVE
* I built this game to learn the Unity environment
* Version 0.2

### How do I get set up? ###

* Go to dowloads tab
* download .zip folder
* extract .zip
* run survive.exe

### Controls ###

* move with wasd
* aim with the mouse
* fire with left mouse button
* press esc to pause the game and spend points to level up
